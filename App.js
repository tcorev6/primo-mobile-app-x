import React from "react";
import { StatusBar } from "react-native";
import Routes from "./src/routes/index";
import { Provider } from "react-redux";
import { configureStore } from "./src/store/createStore";
import axios from "axios";
import PackageDetails from "./src/screens/packageDetails/PackageDetails";
import Confirm from "./src/screens/confirmDetails/ConfirmDetails";
import Snap from "./src/screens/snapPackage/SnapPackage";

axios.interceptors.request.use(
  config => {
    return config;
  },
  error => {
    return Promise.reject(error);
  }
);
const store = configureStore();

const App = () => {
  return (
    <>
      <Provider store={store}>
        <Routes />
      </Provider>
    </>
  );
};

// const App = () => {
//   return (
//     <>
//       <Snap />
//     </>
//   );
// };

export default App;
