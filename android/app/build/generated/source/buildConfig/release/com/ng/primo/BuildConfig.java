/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.ng.primo;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.ng.primo";
  public static final String BUILD_TYPE = "release";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
  // Field from default config.
  public static final String DEV_BASE_URL = "https://primo-test-api.tcore.com.ng/v1";
  // Field from default config.
  public static final String GOOGLE_API_KEY = "AIzaSyBvCUBEdthhyakKyZvU2vJctSBFfjkgUV4";
  // Field from default config.
  public static final String GOOGLE_MAPS_API_KEY = "AIzaSyCokSl0rUpbldTsD-sfTJmHW5y6EqwO8r4;";
  // Field from default config.
  public static final String PROD_BASE_URL = "https://primo-api.tcore.com.ng/v1";
}
